import React from 'react';
import Avatar from '@material-ui/core/Avatar'
import {connect} from 'react-redux'
import {userSignOut} from 'actions/Auth';



class UserInfo extends React.Component {

  render() {
    return (
      <div className="user-profile d-flex flex-row align-items-center">
 
        {/* {JSON.stringify(this.props.userdata.avatar_url)} */}
        <div className="content-user-avatar">
          <Avatar
            alt='...'
            src={this.props.userdata.avatar_url}
            className="user-avatar "
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({settings}) => {
  const {locale} = settings;
  return {locale}
};
export default connect(mapStateToProps, {userSignOut})(UserInfo);


