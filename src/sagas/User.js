import {all, call, put, fork, takeEvery} from "redux-saga/effects";
import {USER_GET_ADVERTISERS} from "constants/ActionTypes";
import {userSetAdvertisers} from "actions/User";
import darriens from '../apis/darriens';


const getUserAdvertisersRequest = async (id, jwt) =>
  await darriens.get(`api/users/${id}/advertisers`, { headers: { Authorization: jwt } })
    .then(response => response)
    .catch(error => error);

function* getUserAdvertisers({payload}) {
  const {id, jwt} = payload;
  try {
    const {data} = yield call(getUserAdvertisersRequest, id, jwt);
    yield put(userSetAdvertisers(data));
  } catch (error) {
    console.log("el error coño: ", error)
  }
}

export function* getAdvertisers() {
  yield takeEvery(USER_GET_ADVERTISERS, getUserAdvertisers);
}

export default function* rootSaga() {
  yield all([
    fork(getAdvertisers)
  ]);
}