import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import {SIGNIN_USER,SIGNOUT_USER,SIGNUP_USER} from "constants/ActionTypes";
import {showAuthMessage,showMessageSuccess, userSignInSuccess, userSignOutSuccess,UserBrand} from "actions/Auth";
import darriens from '../apis/darriens';

const createUserWithEmailPasswordRequest = async (email, password, name) =>
    await darriens.post(`/api/users/`, {
        email,
        password,
        name, profile: 'advertiser', country_id: 'ar'
      })
      .then(response => response)
      .catch(error => error);

const signInUserWithEmailPasswordRequest = async (email, password) =>
    await darriens.post('/api/users/login', {
            email: email,
            password: password
        })
        .then(response => response)
        .catch(error => error);

const SignInVerifyBrand = async (id, jwt) =>
    await darriens.get('api/users/'+id+'/advertisers',{ headers: {Authorization: jwt}} )
        .then(response => response)
        .catch(error => error);

const signOutRequest = async () =>
    await darriens.post('api/users/logout',null,{headers: {Authorization:  localStorage.jwt}})
        .then(response => response)
        .catch(error => error);

function* createUserWithEmailPassword({payload}) {
    const {email, password, name} = payload;
    try {
        const response = yield call(createUserWithEmailPasswordRequest, email, password, name);
        if(response.status === 200) {
            yield put(showMessageSuccess("USER_CREATE_SUCCESS"));
        }else{
            if (response.response.data.hasOwnProperty("error")) {
                yield put(showAuthMessage(response.response.data.error.code));
            } 
        }
    } catch (response) {
        yield put(showAuthMessage(response.data.error.message));
    }
}

function* signInUserWithEmailPassword({payload}) {
    const {email, password} = payload;
    try {
        const signInUser = yield call(signInUserWithEmailPasswordRequest, email, password);
        if (signInUser.data) {
            const verifyBrand = yield call(SignInVerifyBrand, signInUser.data.userId, signInUser.data.id);
            if(verifyBrand.data.length > 0){
                console.log("aqui");
                localStorage.setItem('jwt', signInUser.data.id);
                localStorage.setItem('id',signInUser.data.userId);
                yield put(userSignInSuccess(signInUser.data.id,signInUser.data.userId));
            }
            else{
                console.log("XxXXXX");
                // yield put(push('/brand-create'));
                localStorage.setItem('jwt', signInUser.data.id);
                localStorage.setItem('id',signInUser.data.userId);
                yield put(UserBrand(signInUser.data.id,signInUser.data.userId,true));
            }
        } else {
            if(signInUser.response.data.error.statusCode === 401 || signInUser.response.data.error.statusCode === 400){
                yield put(showAuthMessage(signInUser.response.data.error.code));
            }
        }
    } catch (error) {
        yield put(showAuthMessage(error));
    }
}

function* signOut() {
    try {
        const signOutUser = yield call(signOutRequest);
        if (signOutUser.status === 204) {
            yield put(userSignOutSuccess(signOutUser));
            localStorage.removeItem('jwt');
            localStorage.removeItem('id');
        } else {
            yield put(showAuthMessage(signOutUser.message));
        }
    } catch (error) {
        yield put(showAuthMessage(error));
    }
}

export function* createUserAccount() {
    yield takeEvery(SIGNUP_USER, createUserWithEmailPassword);
}

export function* signInUser() {
    yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* signOutUser() {
    yield takeEvery(SIGNOUT_USER, signOut);
}

export default function* rootSaga() {
    yield all([fork(signInUser),
        fork(createUserAccount),
        fork(signOutUser)]);
}