import {
    HIDE_MESSAGE,
    HIDE_MESSAGE_SUCCESS,
    INIT_URL,
    ON_HIDE_LOADER,
    ON_SHOW_LOADER,
    SHOW_MESSAGE,
    SHOW_MESSAGE_SUCCESS,
    SIGNIN_USER,
    SIGNIN_USER_SUCCESS,
    SIGNOUT_USER,
    SIGNOUT_USER_SUCCESS,
    SIGNUP_USER,
    // SIGNUP_USER_SUCCESS,
    USER_BRAND,
    USER_SET_INFO,
    USER_GET_INFO
} from 'constants/ActionTypes';

export const userSignUp = (user) => {
    return {
        type: SIGNUP_USER,
        payload: user
    };
};
export const userSignIn = (user) => {
    return {
        type: SIGNIN_USER,
        payload: user
    };
};
export const userSignOut = () => {
    return {
        type: SIGNOUT_USER
    };
};

// export const userSignUpSuccess = (authUser) => {
//     return {
//         type: SIGNUP_USER_SUCCESS,
//         payload: authUser
//     };
// };

export const userSignInSuccess = (authUser,id) => {
    return {
        type: SIGNIN_USER_SUCCESS,
        payload: authUser,
        userId:id
    }
};
export const UserBrand = (authUser,id,value) => {
    return {
        type: USER_BRAND,
        payload: authUser,
        userId:id,
        brand:value
    }
};

export const userSignOutSuccess = () => {
    return {
        type: SIGNOUT_USER_SUCCESS,
    }
};

export const showAuthMessage = (message) => {
    return {
        type: SHOW_MESSAGE,
        payload: message
    };
};

export const showMessageSuccess = (message) => {
    return {
        type: SHOW_MESSAGE_SUCCESS,
        payload: message
    };
};

export const setInitUrl = (url) => {
    return {
        type: INIT_URL,
        payload: url
    };
};
export const showAuthLoader = () => {
    return {
        type: ON_SHOW_LOADER,
    };
};

export const hideMessage = () => {
    return {
        type: HIDE_MESSAGE,
    };
};

export const hideMessageSuccess = () => {
    return {
        type: HIDE_MESSAGE_SUCCESS,
    };
};
export const hideAuthLoader = () => {
    return {
        type: ON_HIDE_LOADER,
    };
};
