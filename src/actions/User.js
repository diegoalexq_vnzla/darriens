import {
  USER_GET_INFO,
  USER_SET_INFO,
  USER_GET_ADVERTISERS,
  USER_SET_ADVERTISERS
} from 'constants/ActionTypes';

export const userGetAdvertisers = (user) => {
  return {
    type: USER_GET_ADVERTISERS,
    payload: user
  };
};

export const userSetAdvertisers = (advertisers) => {
  return {
    type: USER_SET_ADVERTISERS,
    payload: advertisers
  };
};

export const userGetInfo = (user) => {
  return {
    type: USER_GET_INFO,
    payload: user
  };
};

export const userSetInfo = (user) => {
  return {
    type: USER_SET_INFO,
    payload: user
  };
};