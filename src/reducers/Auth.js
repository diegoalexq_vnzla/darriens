import {
    HIDE_MESSAGE,
    HIDE_MESSAGE_SUCCESS,
    INIT_URL,
    ON_HIDE_LOADER,
    ON_SHOW_LOADER,
    SHOW_MESSAGE,
    SHOW_MESSAGE_SUCCESS,
    SIGNIN_USER_SUCCESS,
    SIGNOUT_USER_SUCCESS,
    SIGNUP_USER_SUCCESS,
    USER_BRAND
} from "constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    alertMessage: '',
    successMessage: '',
    showMessage: false,
    showMessageSuccess:false,
    brand:false,
    initURL: '',
    authUser: localStorage.getItem('jwt'),
    userId: localStorage.getItem('id')
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SIGNUP_USER_SUCCESS: {
            return {
                ...state,
                loader: false,
                initURL: '/signin',
                authUser: action.payload
            }
        }
        case USER_BRAND: {
            return {
                ...state,
                loader: false,
                initURL: '/brand-create',
                authUser: action.payload,
                brand:action.brand
            }
        }
        case SIGNIN_USER_SUCCESS: {
            return {
                ...state,
                loader: false,
                authUser: action.payload,
                userId:action.userId
            }
        }
        case INIT_URL: {
            return {
                ...state,
                initURL: action.payload
            }
        }
        case SIGNOUT_USER_SUCCESS: {
            return {
                ...state,
                authUser: null,
                initURL: '/signin',  ///app/dashboard/default  LO CAMBIE MANDABA A ESA PAGINA FRUTA
                loader: false
            }
        }

        case SHOW_MESSAGE: {
            return {
                ...state,
                alertMessage: action.payload,
                showMessage: true,
                loader: false
            }
        }
        case SHOW_MESSAGE_SUCCESS: {
            return {
                ...state,
                successMessage: action.payload,
                showMessageSuccess: true,
                loader: false
            }
        }

        case HIDE_MESSAGE: {
            return {
                ...state,
                alertMessage: '',
                showMessage: false,
                loader: false
            }
        }
        case HIDE_MESSAGE_SUCCESS: {
            return {
                ...state,
                successMessage: '',
                showMessageSuccess: false,
                loader: false
            }
        }
        case ON_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }
        case ON_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }
        default:
            return state;
    }
}
