import {
  USER_SET_INFO,
  USER_SET_ADVERTISERS
} from "constants/ActionTypes";

export default (state = [], action) => {
  switch (action.type) {
    case USER_SET_INFO: {
      return {
        ...state,
        ...action.payload
      }
    }
    case USER_SET_ADVERTISERS:
      return {
        ...state,
        advertisers: action.payload
      }
    default:
      return state;

  }
}