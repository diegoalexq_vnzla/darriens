import React from 'react';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormLabel from '@material-ui/core/FormLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import darriens from '../apis/darriens';
import _ from 'lodash';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  submitButton: {
    margin: '32px auto'
  }
});

class BrandCreate extends React.Component {
  state = {
    categories: [],
    countries: [],
    brand: '',
    country: '',
    selected:[
      {name: 'category1', label: 'Categoria 1', value: ''},
      {name: 'category2', label: 'Categoria 2', value: ''},
      {name: 'category3', label: 'Categoria 3', value: ''}
    ]
  };

  async componentDidMount(){
    const {data} = await darriens.get(`/api/lists/interests`, {headers: { Authorization: localStorage.jwt}});
    this.setState({categories: data});
    this.fetchCountries();
  }

  async fetchCountries(){
    const {data} = await darriens.get(`/api/lists/enabledcountries`,
      {headers: { Authorization: localStorage.jwt}});
    this.setState({countries: data});
  }

  onSubmit = async (e) => {
    e.preventDefault();

    const {brand, country} = this.state;
    if(brand !== '' && country !== ''){
      const categories = _.map(_.filter(this.state.selected, (el) => el.value !==''), 'value');

      const response = await darriens.post(`/api/advertisers`, {
        userId: this.props.user.id,
        brand_name: brand,
        categories,
        country_id: country
      }, { headers: {Authorization: localStorage.jwt}});
    }
  }

  handleChange = event => {
    const {selected} = this.state;
    let index = _.findIndex(selected, { 'name': event.target.name});
    selected[index].value = event.target.value;
    this.setState({ selected });
  };
  renderItems(items){
    return this.state[items].map( (element) => {
      return (
        <MenuItem key={element} value={element}>{element}</MenuItem>
      )
    })
  }
  renderCountries(){
    const { classes } = this.props;
    return (
      <FormControl className={classes.formControl} fullWidth="true" required variant="outlined">
        <Select
          className=""
          value={this.state.country}
          onChange={(e) => this.setState({country: e.target.value})}
          displayEmpty
          inputProps={{
            name: 'country',
            id: 'country',
          }}
          input={
            <OutlinedInput
              name="country"
              id="outlined-age-simple"
            />
          }
        >
          <MenuItem value="">Selecciona un país</MenuItem>
          {this.renderItems('countries')}
        </Select>
      </FormControl>
    )
  }
  renderCategories(){
    const { classes } = this.props;
    if(!this.state.categories.length > 0) return <div></div>;
    return this.state.selected.map( (category, index)=> {
      return (
        <FormControl className={classes.formControl} fullWidth="true">
          <InputLabel htmlFor="">{category.label}</InputLabel>
          <Select
            className=""
            value={this.state.selected[index].value}
            onChange={this.handleChange}
            inputProps={{
              name: `${category.name}`,
              id: `${category.name}`,
            }}
          >
            {this.renderItems('categories')}
          </Select>
        </FormControl>
      )
    })
  }
  render(){
    const { classes } = this.props;

    return (
      <div id="brand-create">
        <div
          className="content container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
          <div className="app-login-main-content w-100" style={{maxHeight:'inherit'}}>

            <div className="brand-create__header">
              <h3>Perfil de Marca</h3>
            </div>

            <div className="avatar-container">
              <Avatar className="size-100" alt="Remy Sharp"
                      src="http://pointdakar.com/wp-content/uploads/2017/11/american-pie-jim-chambre-700x336.jpg"/>
            </div>

            <div className="app-login-content w-100">

              <div className="app-login-form text-center">
                <form className={classes.root} noValidate autoComplete="off" onSubmit={this.onSubmit}>
                  <FormLabel component="legend" style={{textAlign:'left', margin: '8px', padding: '6px 0 7px'}}>Marca</FormLabel>
                  <FormControl className={classes.formControl} required fullWidth="true" variant="outlined">
                    <TextField
                      type="text"
                      placeholder="Nombre de la marca"
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      className="mt-0 mb-2"
                      value={this.state.brand}
                      onChange={(e) => this.setState({brand: e.target.value})}
                    />
                  </FormControl>

                  <FormLabel component="legend" style={{textAlign:'left', margin: '8px', padding: '6px 0 7px'}}>País de origen</FormLabel>
                  {this.renderCountries()}

                  <FormLabel component="legend" style={{textAlign:'left', margin: '8px', padding: '6px 0 7px'}}>Categorias</FormLabel>
                  {this.renderCategories()}

                  <FormControl className={classes.formControl} fullWidth="true">
                    <Button type="submit" size="large" className={classes.submitButton} variant="outlined">
                      Siguiente
                    </Button>
                  </FormControl>
                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({user}) => {
  return {
    user
  }
};

export default withStyles(styles)(
  connect(mapStateToProps)(BrandCreate)
);