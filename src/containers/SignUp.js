import React from 'react';
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
import {Link,withRouter} from 'react-router-dom';
// Redirect no se usa se comenta
import IntlMessages from 'util/IntlMessages';
import {
  hideMessage,
  hideMessageSuccess,
  showAuthLoader,
  userSignUp,
  setInitUrl
} from 'actions/Auth';

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: ''
    }
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 3000);
    }
    if (this.props.showMessageSuccess) {
      setTimeout(() => {
          this.props.hideMessageSuccess();
          this.props.setInitUrl('/signin')
          this.props.history.push('/signin');
      }, 3000);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  render() {
    const {
      name,
      email,
      password
    } = this.state;
    const {showMessage, loader,alertMessage,successMessage,showMessageSuccess} = this.props;
    console.log(this.props.alertMessage);
    console.log(this.props.successMessage);
    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">
          <div className="app-logo-content d-flex align-items-center justify-content-center">
            <Link className="logo-lg" to="/" title="darriens">
              <img className="img_main_logo" src={require("assets/images/login/signUp.png")} alt="darriens" title="darriens"/>
            </Link>
          </div>

          <div className="app-login-content">
            <div className="app-login-header mb-4" style={{textAlign:'center'}}>
              <img className="img_logo_darriens" src={require("assets/images/login/logo_azul_svg.png")} alt="darriens" title="darriens" width="63%"/>
            </div>

            <div className="mb-4">
              <h2><IntlMessages id="appModule.createAccount"/></h2>
            </div>

            <div className="app-login-form">
              <form method="post" action="/">
                <TextField
                  type="text"
                  label={<IntlMessages id="appModule.name"/>}
                  onChange={(event) => this.setState({name: event.target.value})}
                  fullWidth
                  defaultValue={name} 
                  margin="normal"
                  className="mt-0 mb-2"
                />

                <TextField
                  type="email"
                  onChange={(event) => this.setState({email: event.target.value})}
                  label={<IntlMessages id="appModule.email"/>}
                  fullWidth
                  defaultValue={email}
                  margin="normal"
                  className="mt-0 mb-2"
                />

                <TextField
                  type="password"
                  onChange={(event) => this.setState({password: event.target.value})}
                  label={<IntlMessages id="appModule.password"/>}
                  fullWidth
                  defaultValue={password}
                  margin="normal"
                  className="mt-0 mb-4"
                />
                <div className="mb-3 d-flex align-items-center justify-content-between">
                  <Button variant="contained" onClick={() => {
                    this.props.showAuthLoader();
                    this.props.userSignUp({email, password, name});
                  }} color="primary">
                    <IntlMessages
                      id="appModule.regsiter"/>
                  </Button>
                  <a href="/signin"><IntlMessages id="signUp.alreadyMember"/></a>
                </div>
              </form>
            </div>
          </div>

        </div>

        {
          loader &&
          <div className="loader-view">
            <CircularProgress/>
          </div>
        }
      
        {showMessage && NotificationManager.error(<IntlMessages id={"signUp."+alertMessage}/>)}
        {showMessageSuccess && NotificationManager.success(<IntlMessages id={"signUp."+successMessage}/> )}
        
        <NotificationContainer/>
      </div>
    )
  }
}

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage,successMessage, showMessage,showMessageSuccess,authUser} = auth;
  return {loader, alertMessage,successMessage, showMessage,showMessageSuccess, authUser}
};

export default withRouter(connect(mapStateToProps, {
  userSignUp,
  hideMessage,
  hideMessageSuccess,
  showAuthLoader,
  setInitUrl
})(SignUp));
