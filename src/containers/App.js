import React, {Component} from 'react';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import MomentUtils from '@date-io/moment';
import {MuiPickersUtilsProvider} from 'material-ui-pickers';
import {Redirect, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import {IntlProvider} from 'react-intl'
import "assets/vendors/style"
import defaultTheme from './themes/defaultTheme';
import AppLocale from '../lngProvider';

import MainApp from 'app/index';
import SignIn from './SignIn';
import SignUp from './SignUp';
import BrandCreate from './BrandCreate';
import {setInitUrl} from '../actions/Auth';
import RTL from 'util/RTL';
import asyncComponent from 'util/asyncComponent';

const RestrictedRoute = ({component: Component, authUser, ...rest}) =>
  <Route
    {...rest}
    render={
      props => authUser ? <Component {...props} />
        : <Redirect to={{pathname: '/signin', state: {from: props.location}}}/>
      }
  />;

class App extends Component {

  componentWillMount() {
    window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
    // console.log("las props de App: ", this.props);
    if (this.props.initURL === '') {
      //Este location es el /signIn como url de inicio Diego
      this.props.setInitUrl(this.props.history.location.pathname);
    }
  }

  render() {
    // match No se usaba
    const { location, locale, authUser, initURL, isDirectionRTL} = this.props;
    // console.log("initURL:"+initURL);
    // console.log(location.pathname);
    // console.log(authUser);
    if (location.pathname === '/') {
      if (authUser === null) {
        return ( <Redirect to={'/signin'}/> );
      }else if (initURL === '' || initURL === '/' || initURL === '/signin') {
        return ( <Redirect to={'/profile'}/> );
      } else {
        return ( <Redirect to={initURL}/> );
      }
    }else{
      if(location.pathname ==='/brand-create' && authUser === null){
        return ( <Redirect to={'/signin'}/> );
      }
      // else if (brand === true) {
      //   return ( <Redirect to={'/brand-create'}/> );
      // }
    }
    const applyTheme = createMuiTheme(defaultTheme);

    if (isDirectionRTL) {
      applyTheme.direction = 'rtl';
      document.body.classList.add('rtl')
    } else {
      document.body.classList.remove('rtl');
      applyTheme.direction = 'ltr';
    }

    const currentAppLocale = AppLocale[locale.locale];
    // console.log(locale);
    return (
      <MuiThemeProvider theme={applyTheme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}>
            <RTL>
              <div className="app-main">
                <Switch>
                  <RestrictedRoute path={`/profile`} authUser={authUser}
                                   component={MainApp}/>
                  <Route path='/signin' component={SignIn}/>
                  <Route path='/signup' component={SignUp}/>
                  <Route path='/brand-create' component={BrandCreate}/>
                  <Route
                    component={asyncComponent(() => import('components/Error404'))}/>
                </Switch>
              </div>
            </RTL>
          </IntlProvider>
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = ({settings, auth}) => {
  const {sideNavColor, locale, isDirectionRTL} = settings;
  const {authUser, initURL} = auth;
  return {sideNavColor, locale, isDirectionRTL, authUser, initURL}
};

export default connect(mapStateToProps, {setInitUrl})(App);

