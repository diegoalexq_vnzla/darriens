import React from 'react';
import {Link} from 'react-router-dom'
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import IntlMessages from 'util/IntlMessages';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
  hideMessage,
  showAuthLoader,
  userSignIn
} from 'actions/Auth';

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      // email: 'diegoalexq+100@gmail.com',
      // email: '',
      email: 'diegoalexq@gmail.com',
      // password: ''
      password: '123456'
    }
  }

  componentDidUpdate() {
    console.log(this.props)
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  render() {
    const {
      email,
      password
    } = this.state;
    const {showMessage, loader, alertMessage} = this.props;
    console.log(showMessage,alertMessage)
    return (
      <div className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">
        

          <div className="app-logo-content d-flex align-items-center justify-content-center">
            <Link className="logo-lg" to="/" title="darriens">
              <img className="img_main_logo" src={require("assets/images/login/login.png")} alt="darriens" title="darriens"/>
            </Link>
          </div>

          <div className="app-login-content">
            <div className="app-login-header mb-4" style={{textAlign:'center'}}>
              <img className="img_logo_darriens" src={require("assets/images/login/logo_azul_svg.png")} alt="darriens" title="darriens" width="63%"/>
            </div>

            <div className="app-login-form">
              <form>
                <fieldset>
                  <TextField
                    label={<IntlMessages id="appModule.email"/>}
                    fullWidth
                    onChange={(event) => this.setState({email: event.target.value})}
                    defaultValue={email}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                  <TextField
                    type="password"
                    label={<IntlMessages id="appModule.password"/>}
                    fullWidth
                    onChange={(event) => this.setState({password: event.target.value})}
                    defaultValue={password}
                    margin="normal"
                    className="mt-1 my-sm-3"
                  />
                </fieldset>
              </form>
            </div>
              <div className="app-login-bottom mb-3  align-items-center justify-content-between" >
                    <Button className="dtpw-btn-login" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userSignIn({email, password});
                    }} variant="contained" color="primary">
                      <IntlMessages id="appModule.signIn"/>
                    </Button>
                    <Button className="dtpw-link-new-user" variant="outlined" href="/signup">
                      <IntlMessages id="signIn.signUp"/>
                    </Button>
              </div>

          </div>

        </div>
        {
          loader &&
          <div className="loader-view">
            <CircularProgress/>
          </div>
        }
        {showMessage && NotificationManager.error(<IntlMessages id={"signIn."+alertMessage}/> )}
        <NotificationContainer/>
      </div>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser} = auth;
  return {loader, alertMessage, showMessage, authUser}
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader
})(SignIn);
