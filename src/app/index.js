import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import Header from 'components/Header/index';
import Sidebar from 'containers/SideNav/index';
import Footer from 'components/Footer';
// import Tour from '../components/Tour/index';
import {
  ABOVE_THE_HEADER,
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
} from 'constants/ActionTypes';
import {isIOS, isMobile} from 'react-device-detect';
import asyncComponent from '../util/asyncComponent';
import TopNav from 'components/TopNav';
import darriens from '../apis/darriens';
import {userSetInfo, userGetAdvertisers} from "../actions";
import Advertisers from './routes/Advertisers';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      dataUserInfo:[],
      dataAdvertisersInfo:[],
      dataInterestsInfo:[]
    }
  }

  componentWillReceiveProps(props) {
    const {user, history} = props;
    if(user.hasOwnProperty("advertisers")){
      if(!user.advertisers.length > 0){
        //Lo redirijo a componente de creación de marca
        history.push('/brand-create');
      }
    }
  }

  componentDidMount() {
    darriens.get(`/api/users/`+localStorage.id, {headers: {Authorization:  localStorage.jwt}})
      .then((res)=>{
        this.props.userSetInfo(res.data);
        this.setState({dataUserInfo:res.data});
        this.props.userGetAdvertisers({id: this.props.user.id, jwt: localStorage.jwt});
      })
      .catch((error) => {
        console.log(error.response.data.error)
      })

    darriens.get('/api/users/'+localStorage.id+'/advertisers', {headers: {Authorization:  localStorage.jwt}})
      .then((res)=>{
        this.setState({dataAdvertisersInfo:res.data})
      })
      .catch((error) => {
        console.log(error.response.data.error)
      })

    darriens.get('api/lists/interests', {headers: {Authorization:  localStorage.jwt}})
      .then((res)=>{
        this.setState({dataInterestsInfo:res.data})
      })
      .catch((error) => {
        console.log(error.response.data.error)
      })
  }
  

  render() {
    const { drawerType, navigationStyle, horizontalNavPosition} = this.props;
    // match se comento no se usa
    const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';

    //set default height and overflow for iOS mobile Safari 10+ support.
    if (isIOS && isMobile) {
      document.body.classList.add('ios-mobile-view-height')
    }
    else if (document.body.classList.contains('ios-mobile-view-height')) {
      document.body.classList.remove('ios-mobile-view-height')
    }

    return (
      <div className={`app-container ${drawerStyle}`}>
        {/* <Tour/> */}
        <Sidebar userdata = {this.state.dataUserInfo} advertisers = {this.state.dataAdvertisersInfo}/>
        <div className="app-main-container">
        {/* <div>
          {JSON.stringify(this.state.username)}
        </div> */}
          <div className={`app-header ${navigationStyle === HORIZONTAL_NAVIGATION ? 'app-header-horizontal' : ''}`} >
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === ABOVE_THE_HEADER) &&
            <TopNav styleName="app-top-header"/>}
            <Header/>
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === BELOW_THE_HEADER) &&
            <TopNav/>}
          </div>

          <main className="app-main-content-wrapper">
          {/* style={{border:'3px solid red', marginTop:'2%'}} */}
            <div className="app-main-content" >
              <Switch>
                  {/* //Origen y destino mamaguevo pelotudo */}
                  <Route  path={'/profile'} component={(() => <Advertisers userdata = {this.state.dataUserInfo} advertisers = {this.state.dataAdvertisersInfo} interests = {this.state.dataInterestsInfo}/> )} />
                  {/* <Route path={`/profile`} component={asyncComponent(() => import('./routes/Advertisers'))}/> */}

                <Route component={asyncComponent(() => import('components/Error404'))}/>
              </Switch>
            </div>
            <Footer/>
          </main>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({settings, user}) => {
  const {drawerType, navigationStyle, horizontalNavPosition} = settings;
  //const {user} = user;
  return {drawerType, navigationStyle, horizontalNavPosition, user}
};
export default withRouter(connect(mapStateToProps, {userSetInfo, userGetAdvertisers})(App));