import React from 'react';
import { withRouter} from 'react-router-dom';
// Route, Switch no se usan
import {connect} from 'react-redux';
// import ContainerHeader from 'components/ContainerHeader';
import IntlMessages from 'util/IntlMessages';
// import darriens from '../../../apis/darriens';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import darriens from '../../../apis/darriens';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import {userSetInfo, userGetAdvertisers} from "../../../actions";

const MySwal = withReactContent(Swal)
const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Advertisers extends React.Component {
  constructor() {
    super();
    this.state = {
      categories:[],
      categoriesSelected: [],
      countryID:'',
      seletectData:'',
      brandName:'',
      idAdvertisers:0
    }
  }


  componentDidMount(){
    this.props.advertisers.forEach((element,key) => {
      // console.log(element);
      this.setState({categoriesSelected: element.categories })
      this.setState({countryID: element.country_id })
      this.setState({brandName: element.brand_name })
      this.setState({idAdvertisers: element.id })
    });
    this.setState({categories: this.props.interests })
  }
  handleChangeCountry = (event) => {
    this.setState({countryID: event.target.value});
  }
  handleChangeInterests = (event) => {
    console.log(event.target.value)
    console.log(this.state.category_1)
    // this.setState({interests: event.target.value});
  }

  handleSubmit = (e) =>{
    e.preventDefault();
    console.log(this.state.brandName);
    console.log(this.state.categoriesSelected);
      let params = {
        brand_name: this.state.brandName,
        categories: this.state.categoriesSelected,
        country_id: this.state.countryID
      };
      darriens.put('api/advertisers/'+this.state.idAdvertisers,params, {headers: {Authorization:  localStorage.jwt}})
      .then((res)=>{
        this.setState({dataInterestsInfo:res.data});
        MySwal.fire({
          type: 'success',
          title: <p>Modificación Exitosa</p>,
          timer: 2500,
          showConfirmButton: false
        })
      })
      .catch((error) => {
        // console.log(error.response.data.error);
        MySwal.fire({
          type: 'error',
          title: <p>Opps.. Algo anda mal</p>,
          timer: 2500,
          showConfirmButton: false
        })
      })
    console.log("aqui guardamos la data de todos los state??")
  }
  inputChangeHandler = (event) => {
    console.log(event.target.id);
    console.log(event.target.value);
    this.setState({ [event.target.id]: event.target.value });
  }

  handleChange = (e) => {
    var index = this.state.categoriesSelected.indexOf(e.target.name);
    if (index > -1) {
      this.state.categoriesSelected[index] = e.target.value;
    }
    this.setState({categoriesSelected: this.state.categoriesSelected});
  }

  renderCategories(){
    const { classes } = this.props;
    if(!this.state.categories.length > 0) return <div></div>;
    return this.state.categoriesSelected.map( (category)=> {
      return (
        <FormControl className={classes.formControl} fullWidth="true">
          {/* <InputLabel htmlFor="">{category.value}</InputLabel> */}
          <Select
            className="select"
            value={category}
            onChange={this.handleChange}
            inputProps={{
              name: category,
              id: category,
            }}
          >
          {/* key="none" value=""><em>None</em> */}
            <MenuItem key="none" value=""><em>None</em>
            </MenuItem>
            {this.renderItems()}
          </Select>
        </FormControl>
      )
    })
  }
  renderItems(){
    return this.state.categories.map( (category) => {
      return (
        <MenuItem key={category} value={category}>{category}</MenuItem>
      )
    })
  }

  render() {
    const { classes } = this.props;
    
    return (
      <div className="app-wrapper profile">
      {/* <pre>
          {JSON.stringify(this.state,null,2)}
      </pre> */}
        {/* <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.samplePage"/>}/> */}
        <div className="col-6" >
        {/* style={{border:'1px solid red'}} */}
        <form
        onSubmit={this.handleSubmit}>
          <div className="row">
            <Button className="dtpw-btn-create-campaign" onClick={() => {}} variant="contained" color="primary">
              <i class="zmdi zmdi-plus"></i> 
                <IntlMessages id="pages.btn.campaign"/>
            </Button>
          </div>
            <div className="name-brand-pf row" >
              <h1>{this.props.advertisers.map((item) => (<span key={item.id}> {item.brand_name}</span>))}</h1>
            </div>
            <div  className="row">
              <h2 className="mail-pf">
                {this.props.userdata.email}
              </h2>
            </div>
            <div className="change-pass-pf row">
              <a href="">Cambiar contraseña</a>
            </div>
            <div className="country-pf row">
              <span className="title-pf">Pais de Origen:</span>
                <FormControl className={classes.formControl} fullWidth="true">
                  <Select 
                    value={this.state.countryID}
                    onChange={this.handleChangeCountry}
                    >
                    <MenuItem key="ar" value="ar">ARGENTINA</MenuItem>
                    <MenuItem key="cl" value="cl">CHILE</MenuItem>
                  </Select>
                </FormControl>
            </div>
            <div className="category-pf row">
            {/* {JSON.stringify(this.props.userdata)} */}
            {/* {JSON.stringify(this.props.advertisers)} */}
            {/* {JSON.stringify(this.props.interests)} */}
            {/* {JSON.stringify(this.state.categoriesSelected)} */}
              <span className="title-pf">Categorias:</span>
              {this.renderCategories()}
                {/* {this.state.categoriesSelected.map((item,index) => { 
                    // value={this.state[item]} onChange={(event) => this.setState({ [item]: event.target.value })}
                    return (<select value= {item} onChange={(event) => this.inputChangeHandler(event)}>
                                <option value="" />
                                {this.state.interests.map((_item,i) =>
                                  <option key={i}  value={_item}>{_item}</option>
                                )}
                            </select>
                          ) 
                  })} */}
            </div>
            <div className="row">
            <Button type="submit" className="dtpw-btn-advertisers-save" onClick={() => {}} variant="contained" color="primary">
              <IntlMessages id="pages.btn.advertisers.save"/>
            </Button>
          </div>
        </form>
        </div>
          {/* <div className="col-6" style={{border:'1px solid red'}}>
          <Button type="submit" className="dtpw-btn-advertisers-save" onClick={() => {}} variant="contained" color="primary">
                <IntlMessages id="pages.btn.advertisers.save"/>
              </Button><Button type="submit" className="dtpw-btn-advertisers-save" onClick={() => {}} variant="contained" color="primary">
                <IntlMessages id="pages.btn.advertisers.save"/>
            </Button>
          </div> */}


      </div>
    );
  }
}

// export default Advertisers;

// const mapStateToProps = (auth) => {
//   const {user} = auth;
//   return {user}
// };
// export default withRouter(connect(mapStateToProps, {userSetInfo, userGetAdvertisers})(Advertisers));
export default withStyles(styles)(Advertisers);
